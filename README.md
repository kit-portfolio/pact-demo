This project is a sandbox intended to be used as playground for contract testing with Pact.

## 1 - COMPONENTS
Project includes two services: "Movie provider" and "Movie consumer".
* "Movie provider" has a "database" storing information about very strange movies (art-house in da house, bro).
* "Movie consumer" makes a requests for movies to "Movie provider".    

## 2 - USE CASES
Provider can handle three use cases:
1. Valid ID => information about movie will be returned.
2. Invalid ID => error message will be returned.
3. Addressing to non-existent movie => error message will be returned.

## 3 - USE CASE TRIGGERING
Use cases are triggered via process arguments. See the reference below:
1. Valid ID => node ./consumer.js 777
2. Invalid ID => node ./consumer.js anyStringOrSmth
3. Addressing to non-existent movie => node ./consumer.js 111

## 4 - BRANCHES
This project has two branches:
1. "localStorage" - pact is configured to interact with contracts stored in project/pacts
2. "pactBroker" - pact is configured to interact with contracts stored in pact broker (prerequisite: pact broker should be deployed and configured - see corresponding manual for instructions).

## 5 - WORKFLOW
1. In order to start working, you need to start "Movie provider" first (node ./providerService.js ) and then execute "Movie consumer" (node ./consumer.js %movieIDhere%). You can play around and send various values as "movieIDhere".
2. When you get used to it, execute unit tests for "Movie consumer" (npm run test:pact:consumer), the contract will be created during test execution.
3. If you storing contracts locally, skip this step || If you managed to setup pact broker (congratz!) execute contact publishing script (npm run test:pact:publish).
4. Finally you get to real deal! It's time to verify your consumer-driven contract on provider's side. Execute ( npm run test:pact:provider) and BEHOLD the power of Pact contract testing :). To see how Pact manages unauthorized provider changes make some minor changes (e.g. change 'error' to ''Error' in provider's responce).
