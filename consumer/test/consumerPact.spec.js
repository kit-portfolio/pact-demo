const chai = require('chai')
const path = require('path')
const chaiAsPromised = require('chai-as-promised')
const Pact = require('@pact-foundation/pact').Pact
const { somethingLike: like, term } = require('@pact-foundation/pact').Matchers
const expect = chai.expect
const API_PORT = process.env.API_PORT || 9123
const { fetchProviderData } = require('../client')
chai.use(chaiAsPromised)

// Configure and import consumer API
// Note that we update the API endpoint to point at the Mock Service
const LOG_LEVEL = process.env.LOG_LEVEL || 'WARN'

const provider = new Pact({
  consumer: 'Movie consumer',
  provider: 'Movie provider',
  port: API_PORT,
  log: path.resolve(process.cwd(), 'logs', 'pact.log'),
  dir: path.resolve(process.cwd(), 'pacts'),
  logLevel: LOG_LEVEL,
  spec: 2,
})

describe('Sandbox pact', () => {
  before(() => {
    return provider.setup()
  })

  describe('GIVEN movie provider API - ', () => {
    describe('WHEN a call to the \'Movie provider\' is made, - ', () => {
      describe('AND a valid movie ID is provided', () => {
        let movieID = 777;

        before(() => {
          return provider.addInteraction({
            // state: 'Stand by',
            uponReceiving: 'successful request for a movie',
            withRequest: {
              method: 'GET',
              path: '/movies/details',
              query: "movieId=" + movieID,
            },
            willRespondWith: {
              status: 200,
              headers: {
                'Content-Type': 'application/json; charset=utf-8',
              },
              body: {
                title: "Zombieavers",
                releaseYear: "2014",
                director: "Some sick man"
              },
            },
          })
        })

        it('can process the JSON payload from the provider', done => {
          const response = fetchProviderData(movieID)
          done()
        })

        it('should validate the interactions and create a contract', () => {
          return provider.verify()
        })
      })

      describe('AND an non-existent movie ID is provided', () => {
        let movieID = 333;

        before(function () {
          return provider.addInteraction({
            // state: 'Stand by',
            uponReceiving: 'a request for non existent movie',
            withRequest: {
              method: 'GET',
              path: '/movies/details',
              query: "movieId=" + movieID,
            },
            willRespondWith: {
              status: 200,
              headers: {
                'Content-Type': 'application/json; charset=utf-8',
              },
              body: {
                error: like(`Movie with ID \'333\' does not exist`),
              },
            },
          })
        })

        it('can handle an invalid date parameter', done => {
          const response = fetchProviderData(movieID)
          done()
        })

        it('should validate the interactions and create a contract', () => {
          return provider.verify()
        })
      })

      describe('AND no date is provided', () => {
        let movieID = true;

        before(() => {
          return provider.addInteraction({
            // state: 'Stand by',
            uponReceiving: 'a request for a movie with invalid ID',
            withRequest: {
              method: 'GET',
              path: '/movies/details',
              query: "movieId=" + movieID,
            },
            willRespondWith: {
              status: 200,
              headers: {
                'Content-Type': 'application/json; charset=utf-8',
              },
              body: { error: `Invalid movie ID` },
            },
          })
        })

        it('can handle an invalid date parameter', done => {
          const response = fetchProviderData(movieID)
          done()
        })

        it('should validate the interactions and create a contract', () => {
          return provider.verify()
        })
      })
      })
    })

})
