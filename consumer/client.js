const request = require('superagent')
const API_HOST = process.env.API_HOST || 'http://localhost'
const API_PORT = process.env.API_PORT || 9123
const API_ENDPOINT = `${API_HOST}:${API_PORT}`

// Fetch provider data
const fetchProviderData = filmID => {
  return request
    .get(`${API_ENDPOINT}/movies/details`)
    .query({ movieId: filmID})
    .then(
      res => {
        if (!res.body.error) {
          console.log("Movie title: " + res.body.title);
          console.log("Director: " + res.body.director);
          console.log("Year: " + res.body.releaseYear);
        }
        if (res.body.error) {
          console.log(res.body.error);
        }
      }
    )
}

module.exports = {
  fetchProviderData,
}
