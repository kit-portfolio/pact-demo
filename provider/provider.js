const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const server = express()

server.use(cors())
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({ extended: true }))
server.use((_, res, next) => {
  res.header('Content-Type', 'application/json; charset=utf-8')
  next()
})

const moviesDB = {
  666 : {
    title: "Sharktopus",
    releaseYear: "2010",
    director: "Shark & Octopus"
  },
  777 : {
    title: "Zombieavers",
    releaseYear: "2014",
    director: "Some sick man"
  },
  888 : {
    title: "Rubber",
    releaseYear: "2011",
    director: "Michelin"
  }
}

server.get('/movies/details', (req, res) => {
  const movieId = parseInt(req.query.movieId);
  if (!movieId) {
    res.status(200)
    res.json({ error: `Invalid movie ID` })
  }
  else if (!moviesDB[movieId]) {
    res.status(200)
    res.json({ error: 'Movie with ID ' + movieId + ' does not exist' })
  }
  else if (moviesDB[movieId]) {
    res.status(200)
    res.json(moviesDB[movieId])
  }
})

module.exports = {
  server,
  moviesDB,
}
