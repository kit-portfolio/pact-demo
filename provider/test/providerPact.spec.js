const Verifier = require('@pact-foundation/pact').Verifier
const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const { server, dataStore } = require('../provider.js')

server.listen(8081, () => {
  console.log('Provider service listening on http://localhost:8081')
})

// Verify that the provider meets all consumer expectations
describe('Pact Verification', () => {
  it('should validate the expectations of \'Movie consumer\'', () => {
    let opts = {
      provider: 'Movie provider',
      providerBaseUrl: 'http://localhost:8081',
      pactUrls: [
        path.resolve(
          process.cwd(),
          './pacts/movie_consumer-movie_provider.json'
        ),
      ],
    }

    return new Verifier().verifyProvider(opts).then(output => {
      console.log('Pact Verification Complete!')
      console.log(output)
    })
  })
})
